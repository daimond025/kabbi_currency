<?php

require __DIR__ . '/../vendor/autoload.php';

$env = new Dotenv\Dotenv(dirname(__DIR__));
$env->load();

// Instantiate the app
$settings = require __DIR__ . '/../src/config/settings.php';
$app = new \Slim\App(['settings' => $settings]);

// Set up dependencies
require __DIR__ . '/../src/config/dependencies.php';

// set error handlers
require __DIR__ . '/../src/config/error_handlers.php';

// Register middleware
require __DIR__ . '/../src/config/middleware.php';

// Register routes
require __DIR__ . '/../src/config/routes.php';

// Run app
$app->run();