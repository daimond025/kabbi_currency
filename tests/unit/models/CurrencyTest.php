<?php

namespace tests\unit\models;

use PHPUnit\Framework\TestCase;
use models\Currency;

class CurrencyTest extends TestCase
{
    
    public function testAttributesOnCurrency()
    {
        $cur = new Currency('RUB', '643', 2, 'Ruble', ['RF']);
        
        $this->assertEquals('RUB', $cur->getCode());
        $this->assertEquals('643', $cur->getNumber());
        $this->assertEquals(2, $cur->getMinorUnit());
        $this->assertEquals('Ruble', $cur->getName());
        $this->assertEquals(['RF'], $cur->getCountries());
    }
    
    public function testGetCurrencySymbolOnEuro()
    {
        $cur = new Currency('EUR', '978');
        
        $this->assertEquals('€', $cur->getSymbol());
    }
    
}