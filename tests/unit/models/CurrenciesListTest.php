<?php

namespace tests\unit\models;

use PHPUnit\Framework\TestCase;
use models\CurrenciesList;
use models\Currency;

class CurrenciesListTest extends TestCase
{
    
    public function testGetListOfCurrency()
    {
        $curData = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $cur =  new Currency('RUB', '643', 2, 'Ruble', ['RF']);
        
        $curList = new CurrenciesList([$curData, $curData]);
        
        $this->assertEquals([$cur, $cur], $curList->getCurrencies());
    }
    
    public function testGetListOfCyrrencyWithFilterObject()
    {
        $curData = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $cur =  new Currency('RUB', '643', 2, 'Ruble', ['RF']);
        $curList = new CurrenciesList([$curData, 1, '2', new CurrenciesList([])]);
        
        $this->assertEquals([$cur], $curList->getCurrencies());
    }
    
    public function testFindOneByCodeOnListOfCurrency()
    {
        $curData1 = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $curData2 = ['code' => 'EUR', 'number' => '978', 'minorUnit' => 0, 'name' => 'Euro', 'countries' => ['RF']];
        $cur = new Currency('EUR', '978', 0, 'Euro', ['RF']);
        $curList = new CurrenciesList([$curData1, $curData2]);
        
        $this->assertEquals($cur, $curList->findOneByCode('EUR'));
    }
    /**
     * @expectedException \exception\NotFoundCurrency
     */
    public function testNotFindByCodeOnListOfCurrency()
    {
        $curData1 = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $curData2 = ['code' => 'EUR', 'number' => '978', 'minorUnit' => 0, 'name' => 'Euro', 'countries' => ['RF']];
        $curList = new CurrenciesList([$curData1, $curData2]);
    
        $curList->findOneByCode('AZN');
    }
    
    public function testFindOneByNumberOnListOfCurrency()
    {
        $curData1 = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $curData2 = ['code' => 'EUR', 'number' => '978', 'minorUnit' => 0, 'name' => 'Euro', 'countries' => ['RF']];
        $cur = new Currency('EUR', '978', 0, 'Euro', ['RF']);
        $curList = new CurrenciesList([$curData1, $curData2]);
        
        $this->assertEquals($cur, $curList->findOneByNumber('978'));
    }
    /**
     * @expectedException \exception\NotFoundCurrency
     */
    public function testNotFindByNumberOnListOfCurrency()
    {
        $curData1 = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $curData2 = ['code' => 'EUR', 'number' => '978', 'minorUnit' => 0, 'name' => 'Euro', 'countries' => ['RF']];
        $curList = new CurrenciesList([$curData1, $curData2]);
    
        $curList->findOneByNumber('971');
    }
    
}