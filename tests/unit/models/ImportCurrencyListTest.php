<?php

namespace tests\unit\models;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use models\CurrenciesList;
use models\ImportCurrencyList;
use models\Currency;

class ImportCurrencyListTest extends TestCase
{

    public function testGetCurrenciesOfCacheUseKeyCurrencyList()
    {
        $curData = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $cacheList = new CurrenciesList([$curData, $curData]);
        $cache = $this->getCacherWithData($cacheList);
        $service = $this->createImportService($this->never());

        $importer = new ImportCurrencyList($cache, $service);
        $list = $importer->import();

        $this->assertInstanceOf('\models\CurrenciesList', $list);
        $this->assertEquals($cacheList, $list);
    }

    private function getCacherWithData($data)
    {
        $cache = new ArrayAdapter();
        $currencyList = $cache->getItem('currencyList');
        $currencyList->set($data);
        $cache->save($currencyList);
        return $cache;
    }

    private function createImportService($expects, $data = null)
    {
        $mock = $this->createMock('\services\ImportService');
        $mock->expects($expects)
             ->method('import')
             ->will($this->returnValue($data));
        return $mock;
    }

    public function testGetCurrenciesOfService()
    {
        $cache = new ArrayAdapter();

        $curData = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $published = new \DateTime();
        $service = $this->createImportService($this->once(), [
            'currencies' => [$curData, $curData],
            'published'  => $published,
        ]);

        $importer = new ImportCurrencyList($cache, $service);
        $list = $importer->import();

        $this->assertInstanceOf('\models\CurrenciesList', $list);
        $this->assertEquals(new CurrenciesList([$curData, $curData], $published), $list);
    }
    
    
    public function testSaveCurrenciesOfCache()
    {
        $cache = new ArrayAdapter();
        $this->assertFalse($cache->getItem('currencyList')->isHit());
        
        $curData = ['code' => 'RUB', 'number' => '643', 'minorUnit' => 2, 'name' => 'Ruble', 'countries' => ['RF']];
        $service = $this->createImportService($this->once(), [
            'currencies' => [$curData, $curData],
            'published' => new \DateTime(),
        ]);
        
        (new ImportCurrencyList($cache, $service))->import();
        
        $this->assertTrue($cache->getItem('currencyList')->isHit());
    }
    
}