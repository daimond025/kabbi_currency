<?php

namespace tests\unit\services\currency_iso;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\ConnectException;
use models\Currency;
use services\currency\HttpImport;

class HttpImportTest extends TestCase
{
    
    public function testGetCurrenciesListOnSuccessful()
    {
        $import = $this->getLoader([
            new Response(200, [], $this->getXmlContents('ruble.xml')),
        ]);
        $this->assertEquals([
            'published' =>  new \DateTime('2016-07-01', new \DateTimeZone('UTC')),
            'currencies' => [
                [
                    'code' => 'RUB',
                    'number' => '643',
                    'minorUnit' => 2,
                    'name' => 'Russian Ruble',
                    'countries' => ['RUSSIAN FEDERATION (THE)']
                ]
            ],
        ], $import->import());
    }
    
    private function getLoader($requests = [])
    {
        $mock = new MockHandler($requests);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        return new HttpImport($client);
    }
    
    /**
     * @expectedException \services\currency\ImportException
     */
    public function testGetCurrenciesListWithErrorXml()
    {
        $loader = $this->getLoader([
            new Response(200, [], $this->getXmlContents('empty.xml')),
        ]);
        $loader->import();
    }
    
    public function testGetCurrenciesListWithErrorOnData()
    {
        $import = $this->getLoader([
            new Response(200, [], $this->getXmlContents('part_ruble.xml')),
        ]);
        $this->assertEquals([
            'published' =>  new \DateTime('2016-07-01', new \DateTimeZone('UTC')),
            'currencies' => [],
        ], $import->import());
    }
    
    public function testGetCurrenciesListWithErrorCurrecncyAttributes()
    {
        $import = $this->getLoader([
            new Response(200, [], $this->getXmlContents('error_ruble.xml')),
        ]);
        $this->assertEquals([
            'published' =>  new \DateTime('2016-07-01', new \DateTimeZone('UTC')),
            'currencies' => [
                [
                    'code' => '',
                    'number' => '',
                    'minorUnit' => 0,
                    'name' => '',
                    'countries' => ['']
                ]
            ],
        ], $import->import());
    }
    
    /**
     * @expectedException \services\currency\ImportException
     */
    public function testTimeoutExceptionOnRequest()
    {
        $loader = $this->getLoader([
            new ConnectException('Timeout', new Request('http://localhost', '/test')),
        ]);
        $loader->import();
    }
    
    public function testGetCurrenciesListWithoutPublieshed()
    {
        $import = $this->getLoader([
            new Response(200, [], $this->getXmlContents('without_published.xml')),
        ]);
        
        $data = $import->import();
        
        $this->assertArrayHasKey('published', $data);
        $this->assertNull($data['published']);
    }
    
    protected function getXmlContents($filename)
    {
        return file_get_contents(__DIR__ . '/data/' . $filename);
    }
}