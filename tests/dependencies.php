<?php

$c = $app->getContainer();

$c['cache'] = function () {
    return new \Symfony\Component\Cache\Adapter\ArrayAdapter(10);
};