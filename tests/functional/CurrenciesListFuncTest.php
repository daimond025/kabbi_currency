<?php

namespace tests\functional;

class CurrenciesListFuncTest extends BaseTestCase
{

    public function testGetAllCurrenciesCorrectAttribute()
    {
        $response = $this->runApp('GET', '/currencies');

        $this->assertEquals(200, $response->getStatusCode());
        
        $json = (string)$response->getBody();
        
        $this->assertJson($json);
        $this->assertContains('published', $json);
        $this->assertContains('count', $json);
        $this->assertContains('currencies', $json);
    }

    /**
     * Test that the index route won't accept a post request
     */
    public function testAllCurrenciesNotAllowed()
    {
        $response = $this->runApp('POST', '/currencies', []);

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message":"Not allowed this method: POST"}', (string)$response->getBody());
    }
    
}