<?php

namespace tests\functional;

class CurrencyFuncTest extends BaseTestCase
{
    
    public function testCurrencyOfRussiaWithNumber()
    {
        $response = $this->runApp('GET', '/currencies/643');
    
        $this->assertEquals(200, $response->getStatusCode());
    
        $json = (string)$response->getBody();
    
        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString($json,
            '{"code":"RUB","number":"643","name":"Russian Ruble","minor_unit":2,' .
            '"countries_list":["RUSSIAN FEDERATION (THE)"],"symbol":"RUB"}');
    }
    
    public function testCurrencyOfAzerbaijanWithCode()
    {
        $response = $this->runApp('GET', '/currencies/azn');
    
        $this->assertEquals(200, $response->getStatusCode());
    
        $json = (string)$response->getBody();
    
        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString($json,
            '{"code":"AZN","number":"944","name":"Azerbaijan Manat",' .
            '"minor_unit":2,"countries_list":["AZERBAIJAN"],"symbol":"AZN"}');
    }
    
    public function testCurrencyNotExists()
    {
        $response = $this->runApp('GET', '/currencies/err');
    
        $this->assertEquals(400, $response->getStatusCode());
    
        $json = (string)$response->getBody();
    
        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('{"message":"Not found currency: err"}', $json);
    }

    public function testCurrencyNotAllowed()
    {
        $response = $this->runApp('POST', '/currencies/643', []);

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString('{"message":"Not allowed this method: POST"}', (string)$response->getBody());
    }
    
}