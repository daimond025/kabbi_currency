<?php

namespace services\currency;

class ImportException extends \Exception
{
    
    public function __construct($message = 'Import Exception', $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
    
}