<?php
namespace services\currency;

use services\ImportService;

class HttpImport implements ImportService
{
    
    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;
    
    public function __construct($client)
    {
        if ($client instanceof \GuzzleHttp\Client) {
            $this->httpClient = $client;
        } elseif(is_string($client)) {
            $this->httpClient = new \GuzzleHttp\Client(['base_uri' => $client]);
        } else {
            throw new \InvalidArgumentException('client must be of string or instance of \GuzzleHttp\Client');
        }
    }
    
    public function import()
    {
        $xml = $this->getXmlData();
        
        if ($time = $xml->attributes()->Pblshd) {
            $time = new \DateTime((string) $time, new \DateTimeZone('UTC'));
        }
        
        $currencies = $this->parseXml($xml);
        
        return [
            'published' => $time,
            'currencies' => $this->groupCountry($currencies),
        ];
    }
    /**
     * Get currency list on a http request
     * @throws LoaderException
     * @return \SimpleXMLElement
     */
    private function getXmlData()
    {
        try {
            $response = $this->httpClient->request('GET', '')->getBody();
            return new \SimpleXMLElement($response);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new ImportException('Fail connect to uri', $e);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            throw new ImportException('Response status code is 4xx', $e);
        } catch (\GuzzleHttp\Exception\SeverException $e) {
            throw new ImportException('Response status code is 5xx', $e);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw new ImportException('Illegal error on request', $e);
        } catch (\Exception $e) {
            throw new ImportException('Response is not xml', $e);
        }
    }
    /**
     *
     * @param \SimpleXMLElement $xml
     * @throws LoaderException
     * @return \models\Currency[]
     */
    private function parseXml(\SimpleXMLElement $xml)
    {
        $currencies = $xml->xpath('CcyTbl/CcyNtry');
        if (! $currencies) {
            throw new ImportException('Not found currencies');
        }
        return array_filter(array_map(function ($xml) {
            if ($xml->count() == 5) {
                return [
                    'code' => (string) $xml->Ccy,
                    'number' => (string) $xml->CcyNbr,
                    'minorUnit' => (integer) $xml->CcyMnrUnts,
                    'name' => (string) $xml->CcyNm,
                    'country' => (string) $xml->CtryNm
                ];
            }
        }, $currencies));
    }
    
    private function groupCountry($currencies)
    {
        $gCurrency = [];
        while ($currencies) {
            $cur = current($currencies);
            unset($cur['country']);
            
            $group = [];
            $diff = [];
            foreach ($currencies as $nextCur) {
                $next = $nextCur;
                unset($next['country']);
                if ($next == $cur) {
                    $group[] = $nextCur; 
                } else {
                    $diff[] = $nextCur;
                }
            }
            
            $cur['countries'] = array_map(function($item) {
                return $item['country'];
            }, $group);
            
            $gCurrency[] = $cur;
            $currencies = $diff;
        }
        return $gCurrency;
    }
    
}