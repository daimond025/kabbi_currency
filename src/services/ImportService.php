<?php
namespace services;

interface ImportService
{
    
    /**
     * @return array 
     */
    public function import();
    
}