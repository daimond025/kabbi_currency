<?php

namespace models;

class Currency implements \JsonSerializable
{
    /**
     * List countries
     * @var array
     */
    private $coutries;
    /**
     * Public name currency
     * @var string
     */
    private $name;
    /**
     * Symbol code currency, e.g. `RUB`
     * @var string
     */
    private $code;
    /**
     * Numeric code currency, e.g. `043`
     * @var string 
     */
    private $number;
    /**
     * Minimal value of coins, e.g. where minorUnit = 2, then minimal value 0.01 RUB
     * @var integer 
     */
    private $minorUnit;
    
    
    public function __construct($code, $number,
            $minorUnit = 0, $name = '', array $countries = []
    ) {
        $this->code = $code;
        $this->number = $number;
        $this->minorUnit = $minorUnit;
        $this->name = $name;
        $this->coutries = $countries;
    }
    
    public function getCountries()
    {
        return $this->coutries;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getCode()
    {
        return $this->code;
    }
    
    public function getNumber()
    {
        return $this->number;
    }
    
    public function getMinorUnit()
    {
        return $this->minorUnit;
    }
    
    public function jsonSerialize()
    {
        return [
            'code' => $this->code,
            'number' => $this->number,
            'name' => $this->name,
            'minor_unit' => $this->minorUnit,
            'countries_list' => $this->coutries,
            'symbol' => $this->getSymbol(),
        ];
    }
    
    public function getSymbol()
    {
        $formatter = new \NumberFormatter('en_US@currency=' . $this->code, \NumberFormatter::CURRENCY);
        return $formatter->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }

}