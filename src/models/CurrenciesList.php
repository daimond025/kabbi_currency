<?php

namespace models;

use exception\NotFoundCurrency;

class CurrenciesList implements \JsonSerializable
{
    /**
     * @var array 
     */
    private $currencies = [];
    
    /**
     * 
     * @var \DateTime
     */
    private $published;
    
    public function __construct(array $currencies, \DateTime $published = null)
    {
        $this->currencies = array_filter(array_map(function ($cur) {
            if (is_array($cur) && 
                key_exists('code', $cur) &&
                key_exists('number', $cur)
            ) {
                return new \models\Currency(
                    $cur['code'],
                    $cur['number'],
                    key_exists('minorUnit', $cur) ? $cur['minorUnit'] : 0,
                    key_exists('name', $cur) ? $cur['name'] : 'No name',
                    key_exists('countries', (array) $cur) ? $cur['countries'] : []
                );
            }
        }, $currencies));
        $this->published = $published ? $published : new \DateTime();
    }
    
    /**
     * 
     * @return Currency[]
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }
    
    public function findOneByCode($code)
    {
        foreach ($this->currencies as $currency) {
            if (! strcasecmp($currency->getCode(), $code)) {
                return $currency;
            }
        }
        throw new NotFoundCurrency($code);
    }
    
    public function findOneByNumber($number)
    {
        foreach ($this->currencies as $currency) {
            if ($currency->getNumber() == $number) {
                return $currency;
            }
        }
        throw new NotFoundCurrency($number);
    }
    
    public function jsonSerialize()
    {
        return [
            'published' => $this->published->format('Y-m-d'),
            'count' => count($this->currencies),
            'currencies' => $this->currencies,
        ];
    }
}