<?php
namespace models;

use Psr\Cache\CacheItemPoolInterface;
use services\ImportService;

/**
 * Импортирует список валют(CurrencyList) из кэша или удаленного сервиса 
 * @author universal
 */
class ImportCurrencyList
{
    
    const CACHE_LIFETIME = 86400;
    
    private $cache;
    
    private $service;
    
    public function __construct(CacheItemPoolInterface $cache, ImportService $service)
    {
        $this->cache = $cache;
        $this->service = $service;
    }
    
    public function import()
    {
        $currencyList = $this->cache->getItem('currencyList');
        if (! $currencyList->isHit()) {
            $data = $this->service->import();
            
            $currencyList->set(new CurrenciesList($data['currencies'], $data['published']));
            $currencyList->expiresAfter(self::CACHE_LIFETIME);
            
            $this->cache->save($currencyList);
        } 
        return $currencyList->get();
    }
    
}