<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/*@var $app \Slim\App */


// Logger

$app->add(function (Request $request, Response $response, $next) {
    $loggerStart;
    $response = $next($request, $response);
    $loggerEnd;
    return $response;
});

