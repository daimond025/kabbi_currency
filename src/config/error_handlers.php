<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$c = $app->getContainer();
$c['errorHandler'] = function ($c) {
    return function (Request $request, Response $response, \Exception $e) {
        return $response->withStatus(400)->withJson([
            'message' => $e->getMessage()
        ]);
    };
};

$c['notFoundHandler'] = function ($c) {
    return function (Request $request, Response $response) {
        return $response->withStatus(404)->withJson([
            'message' => sprintf('Not found request target: %s', $request->getRequestTarget()), 
        ]);
    };
};

$c['notAllowedHandler'] = function ($c) {
    return  function (Request $request, Response $response, $methods) {
        return $response
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withJson([
               'message' => sprintf('Not allowed this method: %s', $request->getMethod()),
            ]);
    };
};