<?php

$c = $app->getContainer();
$c['foundHandler'] = function () {
    return new \Slim\Handlers\Strategies\RequestResponseArgs();
};

$c['currenciesLoader'] = function ($c) {
    $uri = 'http://www.currency-iso.org/dam/downloads/lists/list_one.xml';

    return new \services\currency\HttpImport($uri);
};

$c['cache'] = function ($c) {
    // see https://github.com/nrk/predis/wiki/Connection-Parameters
    $redis = new \Predis\Client([
        'host'         => getenv('REDIS_CACHE_HOST'),
        'port'         => getenv('REDIS_CACHE_PORT'),
        'database'     => getenv('REDIS_CACHE_DATABASE_MAIN'),
        'throw_errors' => true,
    ]);

    return new \Symfony\Component\Cache\Adapter\RedisAdapter($redis, '', 10);
};

