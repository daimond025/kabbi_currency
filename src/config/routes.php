<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/*@var $app \Slim\App */

// Routes JSON

$app->group('/currencies', function () use ($app)  {
    
    $app->get('', '\controllers\CurrencyController:getList');
    $app->get('/{currency}', '\controllers\CurrencyController:get');
    
});