<?php

namespace controllers;

use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use models\CurrenciesList;
use models\ImportCurrencyList;

class CurrencyController
{
    /**
     * @var \models\ImportCurrencyList
     */   
    private $importCurrencyList;
    
    public function __construct(ContainerInterface $ci)
    {
        $currencyService = $ci->get('currenciesLoader');
        $cache = $ci->get('cache');
        
        $this->importCurrencyList = new ImportCurrencyList($cache, $currencyService);
    }

    public function getList(Request $request, Response $response)
    {
        $curList = $this->importCurrencyList->import();
        return $response->withJson($curList);
    }
    
    public function get(Request $request, Response $response, $code)
    {
        $curList = $this->importCurrencyList->import();
        $currency = ctype_digit($code) ? $curList->findOneByNumber($code) 
                                       : $curList->findOneByCode($code);
        return $response->withJson($currency);
    }
    
}