<?php

namespace exception;

class NotFoundCurrency extends \LogicException
{
    public function __construct ($currency, $previous = null)
    {
        $message = sprintf('Not found currency: %s', $currency);
        parent::__construct($message, 0, $previous);
    }    
}