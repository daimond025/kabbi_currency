/**
 * 
 */

var fs = require('fs');
var mockserver = require('mockserver-grunt');
var mockServerClient = require('mockserver-client').mockServerClient;
var HTTP_PORT = 1080;

mockserver.start_mockserver({
	serverPort: HTTP_PORT
}).then(function(){
	mockServerClient('localhost', HTTP_PORT).mockAnyResponse({
		'httpRequest': {
			'method': 'GET',
			'path': '/currency.xml'
		},
		'httpResponse': {
            'statusCode': 200,
			'headers': [
	            {
	            	'name': 'Content-Type',
	            	'values': ['application/xml; charset=utf-8']
	            },
	            {
	            	'name': 'Cache-Control',
	            	'values': 'max-age=0'
	            }
            ],
            'body': fs.readFileSync('data/list_one.xml', 'utf-8'),
		},
		'times': {
			'unlimited': true
		}
	})
	mockServerClient('localhost', HTTP_PORT).mockAnyResponse({
		'httpRequest': {
			'path': '/currencies/643',
		},
		'httpResponse': {
			'headers': [
	            {
	            	'name': 'Content-Type',
	            	'value': ['application/json; charset=utf-8']
	            }
            ],
            'statusCode': 200,
            'body': JSON.stringify({
            	working: 'ok'
            }),
		},
		'times': {
			'unlimited': true
		}
	})
});

console.log("started on port: " + HTTP_PORT);

//stop MockServer if Node exist abnormally
process.on('uncaughtException', function (err) {
	console.log('uncaught exception - stopping node server: ' + JSON.stringify(err));
	mockserver.stop_mockserver();
	throw err;
});

//stop MockServer if Node exits normally
process.on('exit', function () {
	console.log('exit - stopping node server');
	mockserver.stop_mockserver();
});

//stop MockServer when Ctrl-C is used
process.on('SIGINT', function () {
	console.log('SIGINT - stopping node server');
	mockserver.stop_mockserver();
	process.exit(0);
});

//stop MockServer when a kill shell command is used
process.on('SIGTERM', function () {
	console.log('SIGTERM - stopping node server');
	mockserver.stop_mockserver();
	process.exit(0);
});